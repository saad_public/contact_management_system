/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.saad.contactmanagement.ui;

import com.saad.contactmanagement.controllers.MainController;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author witsy_000
 */
public class ApplicationLauncher extends Application{
    
    MainController mainController = null;
    public static Stage stage;
    public void start(Stage stage) throws Exception {
        System.out.println("In start Method");
        //Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        FXMLLoader loader= new FXMLLoader(getClass().getResource("FXMLDocument.fxml"));
        System.out.println("Calling Scene");
        Parent root = loader.load();
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        FXMLDocumentController loginuicontroller =
                loader.<FXMLDocumentController>getController();
        this.mainController = new MainController();
        loginuicontroller.setController(mainController);
        System.out.println("Calling Show Method");
        stage.show();
    }
    //public void beginApp()
    //{
    //    
    //    
    //    this.mainController = new MainController();
    //    System.err.println("In start Method");
    //    launch();
    ///}
}
