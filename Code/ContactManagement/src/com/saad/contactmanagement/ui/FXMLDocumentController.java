/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.saad.contactmanagement.ui;

import com.saad.contactmanagement.controllers.InterfaceMainController;
import com.saad.contactmanagement.controllers.UsersController;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
/**
 *
 * @author witsy_000
 */
public class FXMLDocumentController implements Initializable {
                    
    InterfaceMainController mainController = null;    
  @FXML
    private TextField userid;

    @FXML
    private PasswordField password;

    @FXML
    private Label loglabel;

    
    @FXML
    private void handleButtonAction(ActionEvent event) throws IOException {
        String user=userid.getText();
        String pass = password.getText();
        //UsersController uc = new UsersController();
        //uc.checkValidity(user, pass);
        boolean ck = mainController.checkLogin(user,pass);
        if(ck==false)
        {
            loglabel.setText("wrong username or password");
        }
        else
        {
            cont_home();
        }
    
    }
    /**
     *
     * @throws IOException
     */
    public void cont_home() throws IOException
    {
         
        Parent root1 = FXMLLoader.load(getClass().getResource("contacthome.fxml"));
        Scene scene = new Scene(root1);
        Stage st = new Stage();
        st.setScene(scene);
        ApplicationLauncher.stage.close();
        st.show();
    }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    void setController(InterfaceMainController mc){
        this.mainController = mc;
    }
}
