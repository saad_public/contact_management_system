/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.saad.contactmanagement.controllers;

/**
 *
 * @author witsy_000
 */
public interface InterfaceMainController {
    boolean checkLogin(String userid, String password);
    void addContact(String name,String phone, String address,String email);
}
