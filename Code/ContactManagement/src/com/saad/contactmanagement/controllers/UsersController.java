/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.saad.contactmanagement.controllers;

import com.saad.contactmanagement.dal.DbLayer;
import com.saad.contactmanagement.models.Users;
import com.saad.contactmanagement.ui.FXMLDocumentController;
import java.io.IOException;
import java.sql.DriverManager;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Label;

/**
 *
 * @author witsy_000--
 */
public class UsersController {
    Label loglabel;
    FXMLDocumentController fx = new FXMLDocumentController();
    public boolean checkLogin(String userid, String password)
    {
        //creae a user object
        Users u = new Users();
        u.setUserid(userid);
        u.setPassword(password);
       //then send this object to the DAL
        DbLayer db1 = new DbLayer();
        return db1.userExists(u);
        
    }
    public boolean checkValidity(String userid, String password) throws IOException
    {
        boolean ck = checkLogin(userid,password);
        if(ck==false)
        {
            loglabel.setText("wrong username or password");
        }
        else
        {
            fx.cont_home();
        }
        return ck;
    }     
}
