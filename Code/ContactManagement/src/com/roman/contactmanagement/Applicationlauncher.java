/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.roman.contactmanagement;

import com.roman.contactmanagement.controllers.MainController;
import com.roman.contactmanagement.ui.*;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author LE NOVO
 */
public class Applicationlauncher extends Application 
{
    MainController mainController = null;
     public static Stage stage;
     
     
     @Override
    public void start(Stage stage) throws Exception
    {  
            System.out.println("start method"+ (mainController == null));
            Applicationlauncher.stage=stage;
            FXMLLoader loader= new FXMLLoader(getClass().getResource("/com/roman/contactmanagement/ui/LoginUI.fxml"));
            Parent root = loader.load();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            LoginUIController loginuicontroller = loader.<LoginUIController>getController();
            this.mainController =new MainController();
            loginuicontroller.setController(mainController);
            mainController.setLoginUI(loginuicontroller);
            stage.show();
    }
}
