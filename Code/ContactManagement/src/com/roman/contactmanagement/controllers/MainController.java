/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.roman.contactmanagement.controllers;

import com.roman.contactmanagement.ui.ILoginUI;
import com.roman.contactmanagement.dal.DbLayer;
import com.roman.contactmanagement.models.Users;



/**
 * load login ui
 * @author LE NOVO
 */
public class MainController implements InterfaceMainController 
 {

    ILoginUI loginUI = null;

    @Override
    public void checklogin(String userid, String password) {
                System.out.println("in checklogin method in Maincontroller class"+userid);
        Users u = new Users();
        u.setUserid(userid);
        u.setPassword(password);
        DbLayer db1 = new DbLayer();
        try
        {
            if(db1.userExists(u))
            {
                loginUI.loginSuccess();
            }
            else
            {
                loginUI.loginFailed();
            }

        }
        catch(Exception e)
        {
            System.out.println("Error user exists method " + e.getMessage());
            loginUI.showErrorMessage(e.getMessage());
        }
  
    }

    @Override
    public void addcontact(String name, String phone, String address, String email) {
        //connect to dal
    }
    public void setLoginUI(ILoginUI loginui)
    {
        this.loginUI = loginui;
    }
}
