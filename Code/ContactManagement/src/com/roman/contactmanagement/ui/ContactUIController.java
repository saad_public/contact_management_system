/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.roman.contactmanagement.ui;

import com.roman.contactmanagement.controllers.InterfaceMainController;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author LE NOVO
 */
public class ContactUIController implements Initializable {
    InterfaceMainController mainController = null;   
    
@FXML
    private TextField search;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
    
    void setController(InterfaceMainController mc) 
    {
        this.mainController = mc;
    }
    
}
