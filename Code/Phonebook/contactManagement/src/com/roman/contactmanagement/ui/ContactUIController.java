/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.roman.contactmanagement.ui;

import com.roman.contactmanagement.controllers.InterfaceMainController;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author LE NOVO
 */
public class ContactUIController implements Initializable,IContactUI{
    InterfaceMainController mainController = null;   
    
@FXML
    private TextField search;
@FXML
    private TextField tf_firstname;
@FXML
    private TextField tf_lastname;
@FXML
    private TextField tf_phone;
@FXML
    private TextField tf_email;
@FXML
    private TextField tf_address;
@FXML
    private Label addsuccessmessage;
@FXML
    private Label addfailedmessage;
@FXML
        void addbtnAction(ActionEvent event)
        {
            String first_name=tf_firstname.getText();
            String last_name=tf_lastname.getText();
            String phone=tf_phone.getText();
            String email=tf_email.getText();
            String address=tf_address.getText();
            int i=mainController.addcontact(first_name,last_name,phone,email,address);
            if(i==1)
            {
                cleancontactform();
            }
        }
        
        @FXML
          void searchbtnAction(ActionEvent event)
        {
            System.out.println("search button clicked");
        }
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
    
    void setController(InterfaceMainController mc) 
    {
        this.mainController = mc;
    }
     public void cleancontactform()
        {
                tf_firstname.clear();
                tf_lastname.clear();
                tf_phone.clear();
                tf_email.clear();
                tf_address.clear();
                addsuccessmessage.setText("Contact Added Successfully");
        }
        
        @FXML
        void resetLabel()
        {
                addsuccessmessage.setText("");
        }


    @Override
    public void showErrorMessage(String errmsg) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void loginFailed() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void loginSuccess() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
