/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.roman.contactmanagement.ui;

/**
 *
 * @author nss2710993
 */
public interface IContactUI {
    void showErrorMessage(String errmsg);
    void loginFailed();
    void loginSuccess();
    
}
