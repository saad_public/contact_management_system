/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.roman.contactmanagement.ui;


import com.roman.contactmanagement.Applicationlauncher;
 import com.roman.contactmanagement.controllers.InterfaceMainController;
import com.roman.contactmanagement.controllers.MainController;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 *
 * @author LE NOVO
 */
public class LoginUIController implements Initializable,ILoginUI {

    InterfaceMainController mainController = null;
    
 @FXML
    private TextField userid;

    @FXML
    private PasswordField password;

    @FXML
    private Label loglabel;

    @FXML
    private void handleButtonAction(ActionEvent event) throws IOException {
        String user=userid.getText();
        String pass = password.getText();
        System.out.println("before calling checklogin in LoginUIController class - " + (mainController == null));
        mainController.checklogin(user,pass);
    }
    
    public void loadContactUI()
    {
        System.out.println("login ui is loaded");
        try
        {
                FXMLLoader loader= new FXMLLoader(getClass().getResource("contactUI.fxml"));
                Parent root1 = loader.load();
                Scene scene = new Scene(root1);
                Stage st = new Stage();
                st.setScene(scene);  
                ContactUIController contactuiController = loader.<ContactUIController>getController();
                //this.mainController =new MainController();
                contactuiController.setController(mainController);
                Applicationlauncher.stage.close();
                st.show();
        }
        catch(Exception e)
        {
            System.out.println("Error: "+ e.getMessage());
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    } 
    
    public void setController(InterfaceMainController mc)
    {
        this.mainController =mc;
    }

    @Override
    public void showErrorMessage(String errmsg) {
        // javafx error message dialog
        System.out.println("Error: "+errmsg);
    }

    @Override
    public void loginFailed() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        System.out.println("Login Failed");
        loglabel.setText("wrong username or password");
    }

    @Override
    public void loginSuccess() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        System.out.println("Login Success");
        this.loadContactUI();
    }
    
}
