/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.roman.contactmanagement.dal;

import com.roman.contactmanagement.models.Users;
import com.roman.contactmanagement.models.Contacts;
import com.roman.contactmanagement.utilities.PropertyReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
public abstract class AbstractDBLayer {
    protected Statement st;
    protected String dbhost;
    protected String dbname;
    protected String dbuser;
    protected String dbpass;
    protected String fullconnString="";
    protected Connection con=null;
    
   
  public abstract int addContact2db(Contacts contact);
  public abstract AbstractDBLayer getDBLayer();
  public abstract boolean userExists(Users user)throws Exception;
  public abstract boolean checkInsertContact(Contacts contact)throws Exception;
  public abstract ArrayList<Contacts> getAllContacts()throws Exception;
  public abstract void dbconnect();
  
  // for colsing the connection from DB 
  protected void closeConn()
    {
    try
    {
        if(con!=null)
        {
            con.close();
        }
    }catch(Exception e)
    {
       System.out.println(e.getMessage());
    }
    }
}

/**
 *
 * @author LE NOVO
 */
/*public abstract class AbstractDBLayer {

    protected Statement st;
    
    protected String dbhost;
    protected String dbname;
    protected String dbuser;
    protected String dbpass;
    protected String fullconnString;
    protected Connection con=null;
   
    public AbstractDBLayer dblayer = null;
    public abstract AbstractDBLayer getDblayer();
    public abstract boolean userExists(Users u) throws Exception;
    public abstract int addContact2db(Contacts contact);
    public abstract void dbconnect();
    protected void dbDisconnect() 
    {
        if(con!=null)
        {
            try
            {
                con.close();
            }
            catch (SQLException ex)
            {
                System.out.println(ex.getStackTrace());
            }
        }
    }

    
}
*/