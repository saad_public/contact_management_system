/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.roman.contactmanagement.dal;

import com.roman.contactmanagement.models.Users;
import com.roman.contactmanagement.models.Contacts;
import com.roman.contactmanagement.utilities.PropertyReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author LE NOVO
 */
public class MySQLDbLayer  extends AbstractDBLayer 
{
    
        //AbstractDBLayer dblayer = new MySQLDbLayer();
    

    @Override
    public boolean userExists(Users u) throws Exception
    {
        dbconnect();
        
        
//        try
//        {
            ResultSet result=st.executeQuery("SELECT * FROM users where user_id='"+u.getUserid()+"' and password='"+u.getPassword()+"' ;");
            
            if(result.next())
            {
                if(u.getUserid().equals(result.getString(2)) && u.getPassword().equals(result.getString(3)) )
                {
                        u.setUsertype(result.getString(4));
                        return true;
                 }
            }
//            }
//                catch(Exception e)
//                {
//                    e.printStackTrace();
//                }


        return false;
}
    @Override
    public int addContact2db(Contacts contact)
    {
       
        int result = 0;
        dbconnect();
        try
        {  
            result = st.executeUpdate("insert into contacts (first_name,last_name,phone_number,address,email)values('"+contact.getFirst_name()+"','"+contact.getLast_name()+"','"+contact.getPhone_number()+"','"+contact.getEmail()+"','"+contact.getAddress()+"')");           
        
        }
        catch (SQLException ex)
                {
                        System.out.println("Error is: "+ex.getMessage());
                }
                closeConn();
                return result;
        
    }
    
        
    public void dbconnect()
    {
        try
        {
            //Class.forName("com.mysql.jdbc.Driver");
            dbhost = PropertyReader.getProperty("resources/config.properties", "dbhost");
            dbname = PropertyReader.getProperty("resources/config.properties", "dbname");
            dbuser = PropertyReader.getProperty("resources/config.properties", "dbuser");
            dbpass = PropertyReader.getProperty("resources/config.properties", "dbpass");
            fullconnString = "jdbc:mysql://" + dbhost + "/" + dbname ;
            System.out.println("DBHOSTINFO: "+fullconnString);
            Connection con=DriverManager.getConnection(fullconnString,dbuser,dbpass);
            st = con.createStatement();
        }
        catch(Exception e)
        {
            e.getMessage();
        }
    }

    @Override
    public AbstractDBLayer getDBLayer() {
        return this;
    }

    @Override
    public boolean checkInsertContact(Contacts contact) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<Contacts> getAllContacts() throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}