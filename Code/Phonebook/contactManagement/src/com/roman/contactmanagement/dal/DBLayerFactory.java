/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.roman.contactmanagement.dal;

import com.roman.contactmanagement.utilities.PropertyReader;

/**
 *
 * @author User
 */
public class DBLayerFactory {
    
    static AbstractDBLayer db = null;
    public static AbstractDBLayer getDBLayer() throws Exception
    {
        String dbName = PropertyReader.getProperty("resources/config.properties", "db");
        Class c = Class.forName(dbName);
        Object obj = c.newInstance();
        db = (AbstractDBLayer) obj;
        return db;
        
        /*if("PG".equalsIgnoreCase(dbName))
        {
            return new PostgreSQLLayer.getDBLayer();
        }
        else if("MY".equalsIgnoreCase(dbName))
        {
            return new MySQLDbLayer.getDBLayer();
        }
        else
        {
            throw new Exception("No DB Class specified in config file");
        }*/
    }
    
}