/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.roman.contactmanagement.controllers;

import com.roman.contactmanagement.ui.ILoginUI;
import com.roman.contactmanagement.ui.IContactUI;
import com.roman.contactmanagement.dal.MySQLDbLayer;
import com.roman.contactmanagement.models.Contacts;
import com.roman.contactmanagement.controllers.MainController;
import com.roman.contactmanagement.dal.AbstractDBLayer;
import com.roman.contactmanagement.models.Users;
import com.roman.contactmanagement.utilities.PropertyReader;
import org.apache.log4j.Logger;
//import java.util.logging.Logger;



/**
 * load login ui
 * @author LE NOVO
 */
public class MainController implements InterfaceMainController 
 {

    ILoginUI loginUI = null;
    IContactUI contactUI = null;
    //PropertyReader.getProperty("resources/log4j.properties");
    private static final Logger logger = Logger.getLogger(MainController.class);
    AbstractDBLayer db1 = null;   
    @Override
    public void checklogin(String userid, String password) {
        //System.out.println("in checklogin method in Maincontroller class"+userid);
        logger.debug("In userexists method");
        Users u = new Users();
        u.setUserid(userid);
        u.setPassword(password);
        try
        {
            if(db1.userExists(u))
            {
                logger.info("user exists");
                loginUI.loginSuccess();
            }
            else
            {
                logger.error("Error userexists method - no user found");
                loginUI.loginFailed();
            }

        }
        catch(Exception e)
        {
            logger.error("Error user exists method " + e.getMessage());
            loginUI.showErrorMessage(e.getMessage());
        }
  
    }

    @Override
    public int addcontact(String first_name,String last_name, String phone, String address, String email) {
        //connect to dal
        Contacts contact = new Contacts();
        contact.setFirst_name(first_name);
        contact.setLast_name(last_name);
        contact.setPhone_number(phone);
        contact.setEmail(email);
        contact.setAddress(address); 
        int i=db1.addContact2db(contact);
        logger.debug("maincontroller class result="+i);
        return i;
    }
    public void setLoginUI(ILoginUI loginUI)
    {
        this.loginUI = loginUI;
    }
    public void setContactUI(IContactUI contactUI)
    {
        this.contactUI = contactUI;
    } 
    
    public void setDBLayer(AbstractDBLayer dblayer)
    {
        
        System.out.println("in setDBLayer method in Maincontroller class "+dblayer);
        db1 = dblayer;
    }
}